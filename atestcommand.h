#ifndef ATESTCOMMAND_H
#define ATESTCOMMAND_H

#include <QObject>
#include <QSerialPort>
#include "struct.h"
#include <QTimer>

#define WRITE_TIMEOUT_MS 100
#define READ_TIMEOUT_MS 2000
#define READ_ATTEMPS 1
#define WAIT_TIMEOUT_MS 3000

class ATestCommand : public QObject
{
    Q_OBJECT
protected:
    virtual QByteArray buildRequest()=0;
    virtual bool isPacketFull(QByteArray)=0;
    virtual bool match(QByteArray)=0;  // implement comparison
    std::tuple<QByteArray,uint8_t> read(QSerialPort *s_port);

    QSerialPort m_serial;
    QByteArray rx_buff;
    QTimer m_tm;


   // QByteArray _tx_buff;
public:
    explicit ATestCommand(QObject *parent = nullptr);
    bool execute(portStt portSettings);
    ~ATestCommand() override;
public slots:
    void handleDataReady();
    void handleTimeOut();


signals:
    void match_S(bool ok);
};

#endif // ATESTCOMMAND_H
