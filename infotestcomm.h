#ifndef INFOTESTCOMM_H
#define INFOTESTCOMM_H
#include "atestcommand.h"


class InfoTestComm : public ATestCommand
{

protected:
    QByteArray buildRequest();
    bool isPacketFull(QByteArray);
    bool match(QByteArray);  // implement comparison
public:
    InfoTestComm();
};

#endif // INFOTESTCOMM_H
