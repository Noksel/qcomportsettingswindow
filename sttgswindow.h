#ifndef STTGSWINDOW_H
#define STTGSWINDOW_H

//#include <QMainWindow>
#include <QDialog>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QDebug>

#include <QMetaEnum>
#include <QComboBox>
#include "atestcommand.h"

QT_BEGIN_NAMESPACE
namespace Ui { class SttgsWindow; }
QT_END_NAMESPACE

template<typename QEnum>
QString QtEnumToQString (const QEnum value)
{
    return QString(QMetaEnum::fromType<QEnum>().valueToKey(value));
}

QList<int> QSerialPortStandartDataBits();
QDebug operator<<(QDebug debug, const portStt &stt);


class SttgsWindow : public QDialog
{
    Q_OBJECT

public:
    SttgsWindow(QWidget *parent = nullptr);
    SttgsWindow(QString COMPortName, int baudRate, int dataBits, int flowContorl, int parity, int stopBits, QWidget *parent = nullptr);
    SttgsWindow(const portStt& stt, QWidget *parent = nullptr);
    void setTestComm(ATestCommand* commandObj);
    portStt getSettings();

    ~SttgsWindow() override;

private:
    Ui::SttgsWindow *ui;
    portStt m_pStt;
    bool init();
    bool setCurIndexByIntData(QComboBox *cb,int data);
    ATestCommand* m_commOjb;
    void makeConn();

public slots:
    void handleTestConn();
    void pbApply_clicked();  //auto connection
    void pbDntApply_clicked();  //auto connection
    void CBavlblPorts_currentIndexChanged(int index);
    void CBbaundRates_currentIndexChanged(int index);
    void CBdataBits_currentIndexChanged(int index);
    void CBflowControl_currentIndexChanged(int index);
    void CBparity_currentIndexChanged(int index);
    void CBstopBits_currentIndexChanged(int index);

};
#endif // STTGSWINDOW_H
