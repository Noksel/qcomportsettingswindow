#include "sttgswindow.h"

#include <QApplication>
#include "infotestcomm.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    portStt pstt{"COM3"
                ,QSerialPort::Baud9600
                ,QSerialPort::Data8
                ,QSerialPort::NoFlowControl
                ,QSerialPort::NoParity
                ,QSerialPort::OneStop};

    SttgsWindow w(pstt);  // TEST! last settings or default
    //set test connection and resp handler. true|false
    // w.setTestConnectionCommand("request","resp");
    InfoTestComm *tc = new InfoTestComm();
    w.setTestComm(tc);

    QApplication::connect(&w, &SttgsWindow::accepted,[&w](){

        qDebug()<<"accepted"<<w.result()<<w.getSettings();
    });
     QApplication::connect(&w, &SttgsWindow::rejected,[](){qDebug()<<"rejected";});
     QApplication::connect(&w, &SttgsWindow::finished,[](int res){qDebug()<<"fin hndlr"<<res;});

    w.setModal(true);
    w.show();
    return a.exec();
}
