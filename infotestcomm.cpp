#include "infotestcomm.h"
#include <QtDebug>

QByteArray InfoTestComm::buildRequest()
{
    QString t =QLatin1String("i\r\n");
    return t.toLocal8Bit();
}

bool InfoTestComm::isPacketFull(QByteArray pack)
{
    QByteArray endian("\r\n");
  //  qDebug()<<"check full:"<<pack.right(2)<<endian<<(pack.right(2)==endian);
    return pack.right(2)==endian;
}

bool InfoTestComm::match(QByteArray rx)
{
    QString et ("testDev1.0\r\n");
    return et.toLocal8Bit()== rx;

}

InfoTestComm::InfoTestComm()
{

}
