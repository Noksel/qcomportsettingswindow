#include "atestcommand.h"
#include <QDebug>
#include <QThread>

ATestCommand::ATestCommand(QObject *parent) : QObject(parent)
{
    connect(&m_serial,&QSerialPort::readyRead,this,&ATestCommand::handleDataReady);
    connect(&m_tm,&QTimer::timeout,this,&ATestCommand::handleTimeOut);
}


std::tuple<QByteArray,uint8_t> ATestCommand::read(QSerialPort *s_port)
{
    QByteArray rx_buff;
    qWarning()<<"For Qt 5.15.[0,1] may be not read at all";
    while (s_port->waitForReadyRead(READ_TIMEOUT_MS)){
        rx_buff.append(s_port->readAll());
        if (isPacketFull(rx_buff)){
            return std::make_tuple(rx_buff, 0);
        }
    }
    //   qWarning()<<"REPLACE code for timeOUT";
    return std::make_tuple(rx_buff, s_port->error());  // ERROR TimeOUT!!
}

/*!
 * \brief execute Request, parse Resp and return true|false
 * \param pStt
 * \return
 */
bool ATestCommand::execute(portStt portSettings)
{
    m_serial.setPortName(portSettings.portName);
    m_serial.setSettingsRestoredOnClose(false);
    m_serial.setBaudRate(portSettings.baudRate);
    m_serial.setDataBits(portSettings.dataBits);
    m_serial.setParity(portSettings.parity);
    m_serial.setStopBits(portSettings.stopBits);
    m_serial.setFlowControl(portSettings.flowControl);

    if( m_serial.open(QIODevice::ReadWrite))
        qDebug()<<"open";
    else {
        qDebug()<<"not open";
        emit match_S(false);
        m_serial.clear();
        m_serial.close();
        return false;
    }

    m_serial.setDataTerminalReady(false); // ARDUINO reboot by true
    m_serial.setRequestToSend(true);
    //   QThread::msleep(1500);

    QByteArray rx_buff;

    m_serial.write(buildRequest());
    m_serial.waitForBytesWritten(300);
    m_tm.setInterval(300);
    m_tm.setSingleShot(true);
    m_tm.start();
    return true;
}

ATestCommand::~ATestCommand()
{
    m_serial.flush();
    m_serial.clear();
    m_serial.close();
    rx_buff.clear();
    //        delete m_serial;
    //        m_serial=nullptr;
}

void ATestCommand::handleDataReady()
{
    m_tm.start();
    rx_buff.append(m_serial.readAll());
    if (isPacketFull(rx_buff)){
        m_tm.stop();
        emit match_S(match(rx_buff));
        //    if (m_serial){
        m_serial.flush();
        m_serial.clear();
        m_serial.close();
        rx_buff.clear();
        //            delete m_serial;
        //            m_serial=nullptr;
        //       }
    }
}

void ATestCommand::handleTimeOut()
{
    m_tm.stop();
    qDebug()<<"time out";
    emit match_S(false);
    m_serial.clear();
    m_serial.close();
    //        delete m_serial;
    //        m_serial=nullptr;
}

