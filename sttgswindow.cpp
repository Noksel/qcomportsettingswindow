#include "sttgswindow.h"
#include "ui_sttgswindow.h"

QDebug operator<<(QDebug debug, const portStt &stt)
{
    QDebugStateSaver saver(debug);
    debug<< stt.portName<<stt.baudRate<<stt.dataBits<<stt.flowControl<<stt.parity<<stt.stopBits ;

    return debug;
}

QList<int> QSerialPortStandartDataBits(){
    QMetaEnum a = QMetaEnum::fromType<QSerialPort::DataBits>();
    QList<int> lst;
    for(int i=0;i<a.keyCount();++i) // do not set "-1" - unNoun Data Bits
        lst.append(a.value(i));
    return lst;
}


QList<QString> QSerialPortStandartFlowControl(){
    QMetaEnum a = QMetaEnum::fromType<QSerialPort::FlowControl>();
    QList<QString> lst;
    for(int i=0;i<a.keyCount();++i) // do not set "-1" - unNoun Data Bits
        lst.append(QString(a.key(i)));
    return lst;
}

QList<QString> QSerialPortStandartParity(){
    QMetaEnum a = QMetaEnum::fromType<QSerialPort::Parity>();
    QList<QString> lst;
    for(int i=0;i<a.keyCount();++i) // do not set "-1" - unNoun Data Bits
        lst.append(QString(a.key(i)));
    return lst;
}

QList<QString> QSerialPortStandartStopBits(){
    QMetaEnum a = QMetaEnum::fromType<QSerialPort::StopBits>();
    QList<QString> lst;
    for(int i=0;i<a.keyCount();++i) // do not set "-1" - unNoun Data Bits
        lst.append(QString(a.key(i)));
    return lst;
}


SttgsWindow::SttgsWindow(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::SttgsWindow)
{
    ui->setupUi(this);
    init();
}



SttgsWindow::SttgsWindow(QString COMPortName, int baudRate, int dataBits, int flowContorl, int parity, int stopBits, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::SttgsWindow)
{
    ui->setupUi(this);
    // if(init()){
    init(); // only for test.TOBE REPLACED by if

    int index_CP = ui->CBavlblPorts->findText(COMPortName);
    if (index_CP>-1)
        ui->CBavlblPorts->setCurrentIndex(index_CP);
    else {
        ui->StatusLabel->setText(QString("no %1 Port found").arg(COMPortName));
    }
    qInfo()<<"Set preselected COM Port:"<<(index_CP>-1?"Ok":"Fail");

    qInfo()<<"Set preselected Baud Rate:"<<(setCurIndexByIntData(ui->CBbaundRates,baudRate)?"Ok":"Fail");
    qInfo()<<"Set preselected Data Bits:"<<(setCurIndexByIntData(ui->CBdataBits,dataBits)?"Ok":"Fail");
    qInfo()<<"Set preselected Flow Control:"<<(setCurIndexByIntData(ui->CBflowControl,flowContorl)?"Ok":"Fail");
    qInfo()<<"Set preselected Parity:"<<(setCurIndexByIntData(ui->CBparity,parity)?"Ok":"Fail");
    qInfo()<<"Set preselected Stop Bits:"<<(setCurIndexByIntData(ui->CBstopBits,stopBits)?"Ok":"Fail");
    makeConn();
    //  }
}

SttgsWindow::SttgsWindow(const portStt &stt, QWidget *parent)
    : SttgsWindow(stt.portName,stt.baudRate,stt.dataBits,stt.flowControl,stt.parity,stt.stopBits,parent)
{

}

void SttgsWindow::setTestComm(ATestCommand *commandObj)
{
    m_commOjb=commandObj;
    ui->pbTestConn->setEnabled(true);
}

portStt SttgsWindow::getSettings()
{
    return m_pStt;
}



SttgsWindow::~SttgsWindow()
{
    delete ui;
    if (m_commOjb){
        delete m_commOjb;
    }

}

bool SttgsWindow::init()
{
    QList<QSerialPortInfo>avlblPorts = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &port : qAsConst(avlblPorts))
        ui->CBavlblPorts->addItem(port.portName());

    QList<qint32> avlblBaundRates= QSerialPortInfo::standardBaudRates();
    for (qint32 BR : qAsConst(avlblBaundRates))
        ui->CBbaundRates->addItem(QString::number(BR),BR);


    QList<int> lstDB = QSerialPortStandartDataBits();
    for (int DB: qAsConst(lstDB)){
        ui->CBdataBits->addItem(QString::number(DB),DB);
    }

    QList<QString>lstFC= QSerialPortStandartFlowControl();
    for(const QString &FC:qAsConst(lstFC)){

        ui->CBflowControl->addItem(FC,QMetaEnum::fromType<QSerialPort::FlowControl>().keyToValue(FC.toLocal8Bit()));
    }

    QList<QString>lstPr= QSerialPortStandartParity();
    for(const QString &PR:qAsConst(lstPr)){
        ui->CBparity->addItem(PR,QMetaEnum::fromType<QSerialPort::Parity>().keyToValue(PR.toLocal8Bit()));
    }


    QList<QString>lstSB= QSerialPortStandartStopBits();
    for(const QString &SB:qAsConst(lstSB)){
        ui->CBstopBits->addItem(SB,QMetaEnum::fromType<QSerialPort::StopBits>().keyToValue(SB.toLocal8Bit()));
    }

    ui->CBavlblPorts->model()->sort(0);
    if (avlblPorts.size()<1){
        ui->StatusLabel->setText("no COM Port found");
        return false;
    }
    ui->pbTestConn->setEnabled(true);


    return true;
}

bool SttgsWindow::setCurIndexByIntData(QComboBox *cb, int data)
{
    int index = cb->findData(data);
    if (index>-1){
        cb->setCurrentIndex(index);
        return true;
    }
    return false;
}

void SttgsWindow::makeConn()
{
    connect(ui->pbApply, &QPushButton::clicked,this, &SttgsWindow::pbApply_clicked);  //auto connection
    connect(ui->pbDntApply, &QPushButton::clicked,this, &SttgsWindow::pbDntApply_clicked);  //auto connection
    connect(ui->pbTestConn,&QPushButton::released,this,&SttgsWindow::handleTestConn);

    connect(ui->CBavlblPorts,static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this,&SttgsWindow::CBavlblPorts_currentIndexChanged);
    connect(ui->CBbaundRates,static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, &SttgsWindow::CBbaundRates_currentIndexChanged);
    connect(ui->CBdataBits,static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, &SttgsWindow::CBdataBits_currentIndexChanged);
    connect(ui->CBflowControl,static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, &SttgsWindow::CBflowControl_currentIndexChanged);
    connect(ui->CBparity,static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, &SttgsWindow::CBparity_currentIndexChanged);
    connect(ui->CBstopBits,static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, &SttgsWindow::CBstopBits_currentIndexChanged);

}

void SttgsWindow::handleTestConn()
{
    qDebug()<<"TEST CONNECTION";
    ui->pbTestConn->setIcon(QIcon());
    if (m_commOjb){
        connect(m_commOjb,&ATestCommand::match_S,[this](bool ok){
            if (ok) //--> true|false
                ui->pbTestConn->setIcon(QIcon(":/ok"));
            else
                ui->pbTestConn->setIcon(QIcon(":/fail"));
        });
        m_commOjb->execute(m_pStt);
    }
}



void SttgsWindow::pbApply_clicked()
{
    this->accept();
}

void SttgsWindow::pbDntApply_clicked()
{
    this->reject();
}

void SttgsWindow::CBavlblPorts_currentIndexChanged(int index)
{
    m_pStt.portName = ui->CBavlblPorts->itemText(index);
    ui->pbTestConn->setIcon(QIcon());
}

void SttgsWindow::CBbaundRates_currentIndexChanged(int index)
{
    m_pStt.baudRate = static_cast<QSerialPort::BaudRate>( ui->CBbaundRates->itemData(index).toInt());
    ui->pbTestConn->setIcon(QIcon());
}

void SttgsWindow::CBdataBits_currentIndexChanged(int index)
{
    m_pStt.dataBits = static_cast<QSerialPort::DataBits>( ui->CBdataBits->itemData(index).toInt());
    ui->pbTestConn->setIcon(QIcon());
}

void SttgsWindow::CBflowControl_currentIndexChanged(int index)
{
    m_pStt.flowControl = static_cast<QSerialPort::FlowControl>( ui->CBflowControl->itemData(index).toInt());
    ui->pbTestConn->setIcon(QIcon());
}

void SttgsWindow::CBparity_currentIndexChanged(int index)
{
    m_pStt.parity = static_cast<QSerialPort::Parity>( ui->CBparity->itemData(index).toInt());
    ui->pbTestConn->setIcon(QIcon());
}

void SttgsWindow::CBstopBits_currentIndexChanged(int index)
{
    m_pStt.stopBits = static_cast<QSerialPort::StopBits>( ui->CBstopBits->itemData(index).toInt());
    ui->pbTestConn->setIcon(QIcon());
}

