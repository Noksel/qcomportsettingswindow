#ifndef STRUCT_H
#define STRUCT_H
#include <QString>
#include <QSerialPort>
struct portStt{
    QString portName;
    QSerialPort::BaudRate baudRate;
    QSerialPort::DataBits dataBits;
    QSerialPort::FlowControl flowControl;
    QSerialPort::Parity parity;
    QSerialPort::StopBits stopBits;
};
#endif // STRUCT_H
